# Options Hashes
#
# Write a method `transmogrify` that takes a `String`. This method should
# take optional parameters `:times`, `:upcase`, and `:reverse`. Hard-code
# reasonable defaults in a `defaults` hash defined in the `transmogrify`
# method. Use `Hash#merge` to combine the defaults with any optional
# parameters passed by the user. Do not modify the incoming options
# hash. For example:
#
# ```ruby
# transmogrify("Hello")                                    #=> "Hello"
# transmogrify("Hello", :times => 3)                       #=> "HelloHelloHello"
# transmogrify("Hello", :upcase => true)                   #=> "HELLO"
# transmogrify("Hello", :upcase => true, :reverse => true) #=> "OLLEH"
#
# options = {}
# transmogrify("hello", options)
# # options shouldn't change.
# ```

def transmogrify(message , options = {})
  defaults = {
  times: 1,
  upcase:false,
  reverse:false
  }
  final_message = message
  options = defaults.merge(options)
  if options[:upcase] == true
    final_message = final_message.upcase
  end
  if options[:reverse] == true
    final_message = final_message.reverse
  end
  i = 1
  message_to_add = final_message
  while i < options[:times]
    final_message += message_to_add
    i += 1
  end
  return final_message
end
